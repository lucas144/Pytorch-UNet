import argparse
import logging
import os

import numpy as np
import torch
import torch.nn.functional as F
from PIL import Image
from torchvision import transforms
import cv2

from pyunet.unet import UNet
from pyunet.utils.data_vis import plot_img_and_mask, plot_img_mask_truth, plot_img_mask
from pyunet.utils.dataset import EVTDataset


def predict_img(net,
                full_img,
                device,
                scale_factor=1,
                out_threshold=0.5,
                preprocess=True):
    net.eval()

    if preprocess:
        img = torch.from_numpy(EVTDataset.preprocess(full_img, resize=(750,2200)))
        img = img.unsqueeze(0)
        img = img.to(device=device, dtype=torch.float32)
    else:
        img = full_img.unsqueeze(0)
        img = img.to(device=device, dtype=torch.float32)

    with torch.no_grad():
        output = net(img)

        if net.n_classes > 1:
            probs = F.softmax(output, dim=1)
        else:
            probs = torch.sigmoid(output)


        probs = probs.squeeze(0)

        # TODO: probs has 5 channels so it can't be converted to PIL image, need to change this and resize some other way

        tf = transforms.Compose(
            [
                transforms.ToPILImage(),
                transforms.Resize(full_img.shape[:2]),
                #transforms.Resize(full_img.size[1]),
                transforms.ToTensor()
            ]
        )

        #probs = tf(probs.cpu())

        full_mask = probs.squeeze().cpu().numpy()
        full_mask = np.transpose(full_mask, (1, 2, 0))

        if preprocess:
            full_mask = cv2.resize(full_mask, (full_img.shape[1], full_img.shape[0]))

    #return full_mask > out_threshold
    return full_mask


def get_args():
    parser = argparse.ArgumentParser(description='Predict masks from input images',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--model', '-m', default='pyunet/checkpoints/CP_epoch40.pth',
                        metavar='FILE',
                        help="Specify the file in which the model is stored")
    parser.add_argument('--input', '-i', metavar='INPUT', nargs='+',
                        help='filenames of input images') #, required=True)

    parser.add_argument('--output', '-o', metavar='INPUT', nargs='+',
                        help='Filenames of ouput images')
    parser.add_argument('--viz', '-v', action='store_true',
                        help="Visualize the images as they are processed",
                        default=True)
    parser.add_argument('--no-save', '-n', action='store_true',
                        help="Do not save the output masks",
                        default=True)
    parser.add_argument('--mask-threshold', '-t', type=float,
                        help="Minimum probability value to consider a mask pixel white",
                        default=0.5)
    parser.add_argument('--scale', '-s', type=float,
                        help="Scale factor for the input images",
                        default=0.5)

    return parser.parse_args()


def get_output_filenames(args):
    in_files = args.input
    out_files = []

    if not args.output:
        for f in in_files:
            pathsplit = os.path.splitext(f)
            out_files.append("{}_OUT{}".format(pathsplit[0], pathsplit[1]))
    elif len(in_files) != len(args.output):
        logging.error("Input files and output files are not of the same length")
        raise SystemExit()
    else:
        out_files = args.output

    return out_files


def mask_to_image(mask):
    return Image.fromarray((mask * 255).astype(np.uint8))

def predict_gui(img, model_filename='pyunet/checkpoints/CP_epoch40.pth'):

    # set gpu/cpu device
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    # load UNet
    net = UNet(n_channels=1, n_classes=5)
    net.to(device=device)
    net.load_state_dict(torch.load(model_filename, map_location=device))

    mask = predict_img(net=net,
                       full_img=img,
                       scale_factor=0.5,
                       out_threshold=0.5,
                       device=device)

    return mask


if __name__ == "__main__":
    # get cmd line params
    args = get_args()

    # set gpu/cpu device
    device = torch.device('cuda:1' if torch.cuda.is_available() else 'cpu')
    #device = torch.device('cpu')
    logging.info('Using device {}'.format(device))

    # load UNet
    net = UNet(n_channels=1, n_classes=5)
    net.to(device=device)
    net.load_state_dict(torch.load(args.model, map_location=device))
    logging.info("Loading model {}".format(args.model))

    in_files = args.input
    # if in_files set, then run predictions on those
    if in_files is not None:
        out_files = get_output_filenames(args)
        for i, fn in enumerate(in_files):
            logging.info("\nPredicting image {} ...".format(fn))
            img = cv2.imread(fn, cv2.IMREAD_GRAYSCALE)
            #img = np.expand_dims(img, -1)
            mask = predict_img(net=net,
                               full_img=np.expand_dims(img, -1),
                               scale_factor=args.scale,
                               out_threshold=args.mask_threshold,
                               device=device)

            if not args.no_save:
                out_fn = out_files[i]
                result = mask_to_image(mask)
                result.save(out_files[i])
                logging.info("Mask saved to {}".format(out_files[i]))

            if args.viz:
                logging.info("Visualizing results for image {}, close to continue ...".format(fn))
                #plot_img_and_mask(img, mask)
                plot_img_mask(img, mask)

    # if in_files not set then run on training/testing data
    else:
        img_dir = '/home/ljt/evt/evt-lect-data/data/ucsd/'
        dataset = EVTDataset(img_dir, resize=(750,2200))
        for i in range(len(dataset)):
            # get image and mask from dataset
            image, mask = dataset[i]['image'], dataset[i]['mask']
            # get prediction
            predicted_mask = predict_img(net=net,
                               full_img=image,
                               scale_factor=args.scale,
                               out_threshold=args.mask_threshold,
                               device=device,
                               preprocess=False)

            # visualize original image, ground truth masks and predicted masks
            if args.viz:
                #plot_img_and_mask(image[0].numpy(), predicted_mask)
                plot_img_mask_truth(image[0].numpy(), predicted_mask, mask )
