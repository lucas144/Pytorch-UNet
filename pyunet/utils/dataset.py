from os.path import splitext
from os import listdir
import numpy as np
from glob import glob
import torch
from torch.utils.data import Dataset
import logging
from PIL import Image
import cv2
import random

class EVTDataset(Dataset):
    def __init__(self, imgs_dir, resize=(768,2944)):

        #imgs_dir = '/Users/ltindall/evt/evt-lect-data/data/ucsd/'

        self.imgs_dir = imgs_dir
        quarter_dirs = glob(self.imgs_dir+'/*')
        course_dirs = [c_dir for q in quarter_dirs for c_dir in glob(q+'/*')]
        lecture_dirs = [l_dir for course in course_dirs for l_dir in glob(course+'/*/')]
        self.label_filenames = [f_name for l in lecture_dirs for f_name in glob(l+'/frame*/full/*base.png')]
        self.img_filenames = [glob('/'.join(l.split('/')[:-2])+'/*-original.png')[0] for l in self.label_filenames]

        assert len(self.img_filenames) == len(self.label_filenames), 'Error: # of images does not match # of labels'

        #self.scale = scale
        self.resize = resize
        #assert 0 < scale <= 1, 'Scale must be between 0 and 1'

        #self.ids = [splitext(file)[0] for file in listdir(imgs_dir)
        #            if not file.startswith('.')]
        self.ids = list(range(len(self.img_filenames)))
        random.shuffle(self.ids)

        logging.info('Creating dataset with {} examples'.format(len(self.ids)))

    def __len__(self):
        return len(self.ids)

    @classmethod
    def preprocess(cls, img, resize, mask=False):
        h,w  = img.shape[:2]
        #newW, newH = int(scale * w), int(scale * h)
        #assert newW > 0 and newH > 0, 'Scale is too small'
        newH, newW = resize
        img = cv2.resize(img, (newW, newH))
        if len(img.shape) == 2:
            img = np.expand_dims(img, -1)
        # HWC to CHW
        img_trans = np.transpose(img, (2, 0, 1))
        if mask:
            # 0 => background, 1 => border, 2 => blue erases, 3 => red lines, 4+ => contours
            mask_id = {0: 'background', 1: 'border', 2: 'blue', 3: 'red'}
            #new_mask = np.zeros((4, img_trans.shape[1], img_trans.shape[2]))
            #for i in mask_id.keys():
            #    new_mask[i,:,:] = img_trans[0,:,:] == i
            img_trans[img_trans>=4] = 4 # make all contours = 4
            new_mask = img_trans[0,:,:] # all channels should have same value, so just take one slice
            img_trans = new_mask
        else:
            if img_trans.max() > 1:
                img_trans = img_trans / 255
            # normalize images between -1 to 1
            img_trans = img_trans - 0.5 / 0.5

        return img_trans

    def __getitem__(self, i):
        idx = self.ids[i]
        mask_file = self.label_filenames[idx]
        img_file = self.img_filenames[idx]

        mask = cv2.imread(mask_file)
        img = cv2.imread(img_file, cv2.IMREAD_GRAYSCALE)
        img = np.expand_dims(img, -1)

        assert img.shape[:2] == mask.shape[:2], \
                'Image and mask {},{} should be the same size, but are {} and {}'.format(img_file, mask_file, img.shape[:2], mask.shape[:2])

        img = self.preprocess(img, self.resize)
        mask = self.preprocess(mask, self.resize, mask=True)

        return {'image': torch.from_numpy(img), 'mask': torch.from_numpy(mask)}


class BasicDataset(Dataset):
    def __init__(self, imgs_dir, masks_dir, scale=1):
        self.imgs_dir = imgs_dir
        self.masks_dir = masks_dir
        self.scale = scale
        assert 0 < scale <= 1, 'Scale must be between 0 and 1'

        self.ids = [splitext(file)[0] for file in listdir(imgs_dir)
                    if not file.startswith('.')]
        logging.info('Creating dataset with {} examples'.format(len(self.ids)))

    def __len__(self):
        return len(self.ids)

    @classmethod
    def preprocess(cls, pil_img, scale):
        w, h = pil_img.size
        newW, newH = int(scale * w), int(scale * h)
        assert newW > 0 and newH > 0, 'Scale is too small'
        pil_img = pil_img.resize((newW, newH))

        img_nd = np.array(pil_img)

        if len(img_nd.shape) == 2:
            img_nd = np.expand_dims(img_nd, axis=2)

        # HWC to CHW
        img_trans = img_nd.transpose((2, 0, 1))
        if img_trans.max() > 1:
            img_trans = img_trans / 255

        return img_trans

    def __getitem__(self, i):
        idx = self.ids[i]
        mask_file = glob(self.masks_dir + idx + '.*')
        img_file = glob(self.imgs_dir + idx + '.*')

        assert len(mask_file) == 1, \
            'Either no mask or multiple masks found for the ID {}: {}'.format(idx, mask_file)
        assert len(img_file) == 1, \
            'Either no image or multiple images found for the ID {}: {}'.format(idx, img_file)
        mask = Image.open(mask_file[0])
        img = Image.open(img_file[0])

        assert img.size == mask.size, \
            'Image and mask {} should be the same size, but are {} and {}'.format(idx, img.size, mask.size)

        img = self.preprocess(img, self.scale)
        mask = self.preprocess(mask, self.scale)

        return {
            'image': torch.from_numpy(img).type(torch.FloatTensor),
            'mask': torch.from_numpy(mask).type(torch.FloatTensor)
        }
