import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np


def plot_img_mask(img, prediction):
    n_classes = prediction.shape[2] if len(prediction.shape) > 2 else 1

    fig = plt.figure()
    gs = gridspec.GridSpec(2, n_classes, wspace=0.25, hspace=0.25)
    ax0 = fig.add_subplot(gs[0, :]) # first row
    ax0.set_title('Input image')
    ax0.imshow(img, cmap='gray')

    # 0 => background, 1 => border, 2 => blue erases, 3 => red lines, 4+ => contour
    classes = {0:'background', 1:'border', 2:'blue lines', 3:'red lines', 4:'contours'}
    for i in range(n_classes):
        ax_pred = fig.add_subplot(gs[1,i])
        ax_pred.set_title('{} prediction'.format(classes[i]))
        ax_pred.imshow(prediction[:,:,i], cmap='gray')

    plt.xticks([]), plt.yticks([])
    plt.show()
def plot_img_mask_truth(img, prediction, truth):
    n_classes = prediction.shape[2] if len(prediction.shape) > 2 else 1

    fig = plt.figure()
    gs = gridspec.GridSpec(3, n_classes, wspace=0.25, hspace=0.25)
    ax0 = fig.add_subplot(gs[0, :]) # first row
    ax0.set_title('Input image')
    ax0.imshow(img, cmap='gray')

    # 0 => background, 1 => border, 2 => blue erases, 3 => red lines, 4+ => contour
    classes = {0:'background', 1:'border', 2:'blue lines', 3:'red lines', 4:'contours'}
    for i in range(n_classes):
        ax_truth = fig.add_subplot(gs[1,i])
        ax_truth.set_title('{} ground truth'.format(classes[i]))
        ax_truth.imshow((truth==i)*255, cmap='gray')
        ax_pred = fig.add_subplot(gs[2,i])
        ax_pred.set_title('{} prediction'.format(classes[i]))
        ax_pred.imshow(prediction[:,:,i], cmap='gray')

    plt.xticks([]), plt.yticks([])
    plt.show()

def plot_img_and_mask(img, mask):
    classes = mask.shape[2] if len(mask.shape) > 2 else 1
    fig, ax = plt.subplots(1, classes + 1)
    ax[0].set_title('Input image')
    ax[0].imshow(img)
    if classes > 1:
        for i in range(classes):
            ax[i+1].set_title('Output mask (class {})'.format(i+1))
            ax[i+1].imshow(mask[:, :, i]*255, cmap='gray')
            print('mask = ', np.unique(mask[:,:,i]))

    else:
        ax[1].set_title('Output mask')
        ax[1].imshow(mask)
    plt.xticks([]), plt.yticks([])
    plt.show()
