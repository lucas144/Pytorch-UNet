from setuptools import setup, find_packages

setup(name='pyunet',
      version='0.0.1',
      description='pyunet',
      url='https://gitlab.com/lucas144/Pytorch-UNet',
      packages=find_packages()
)
